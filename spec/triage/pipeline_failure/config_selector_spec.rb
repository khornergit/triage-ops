# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/config_selector'
require_relative '../../../triage/triage/pipeline_failure/config/base'

RSpec.describe Triage::PipelineFailure::ConfigSelector do
  let(:event) do
    instance_double(Triage::PipelineEvent, id: 42)
  end

  describe '.find_config' do
    let(:config_class) do
      Class.new(Triage::PipelineFailure::Config::Base)
    end

    context 'when config class does not implement .match?' do
      it 'raises NotImplementedError' do
        expect { described_class.find_config(event, [config_class]) }
          .to raise_error(NotImplementedError)
      end
    end

    context 'when config class implements .match?' do
      context 'when no config class matches' do
        it 'returns nil' do
          expect(described_class.find_config(event, [])).to be_nil
        end
      end

      context 'when a config class matches' do
        let(:config_class1) do
          Class.new(Triage::PipelineFailure::Config::Base) do
            def self.match?(event)
              event.id == 99
            end
          end
        end

        let(:config_class2) do
          Class.new(Triage::PipelineFailure::Config::Base) do
            def self.match?(event)
              event.id == 42
            end
          end
        end

        it 'returns an object of the first config class for which `.match?` returns true' do
          expect(described_class.find_config(event, [config_class1, config_class2])).to be_an_instance_of(config_class2)
        end
      end
    end
  end
end
