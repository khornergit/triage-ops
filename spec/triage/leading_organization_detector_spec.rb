# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/leading_organization_detector'

RSpec.describe Triage::LeadingOrganizationDetector, :clean_cache do
  let(:username) { 'leader' }
  let(:regular_username) { 'regular' }

  describe '#leading_organization?' do
    let(:credential_path) { '' }
    let(:sheet_id) { '' }

    before do
      allow(ENV).to receive(:fetch).with('LEADING_ORGS_TRACKER_SHEET_ID', '').and_return(sheet_id)

      allow(ENV).to receive(:fetch).with('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH', '').and_return(credential_path)
      # NOTE: This private key is generated and not real!
      file_content = <<-JSON
        {
          "type": "service_account",
          "project_id": "abc123",
          "private_key_id": "1111111111111111111111111111111111111111",
          "private_key": "-----BEGIN PRIVATE KEY-----\\nMIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAv4KNL8dZPL6h0tTP\\nL5WXti24v5osoVv4emCux6S2H4bu3ata8ppqHfcSsJSxiqlBq1sqqx3uVxkZDBuF\\nsJNE9QIDAQABAkBKRE+KUs15cBgDUcHTGzkNTifSLfDW1nrCwpGlHGwARz+3OwKl\\nSFPOVxAhzHdtaHjd/oGL50/qlp3PDJGNPAO1AiEA94o1SVjQ0NBLnJq4Lsm1yU0U\\nvHps+BN+O2jXfyXCXy8CIQDGDh90PZoBBrp5nL120ZeiPwkfObfIds9FP/bfOum1\\nGwIhAJhATJgJZZ4Zj3gJ/aDhdcsTet6WWjGXI7v8txbALbYHAiAaS3O3nhodOsR1\\nMu8goFEOdGoEoEgbMFLycbyYBJ1UswIgX7lVsqWb7WsCslr2kluFzP5aC9wMUvWG\\nUTSIXUN6mqk=\\n-----END PRIVATE KEY-----\\n",
          "client_email": "gitlab@example.com",
          "client_id": "111111111111111111111",
          "auth_uri": "https://accounts.google.com/o/oauth2/auth",
          "token_uri": "https://oauth2.googleapis.com/token",
          "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
          "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/gitlab%40example.iam.gserviceaccount.com",
          "universe_domain": "googleapis.com"
        }
      JSON
      file = instance_double(File, read: file_content)
      allow(File).to receive(:open).with(credential_path).and_return(file)
      stub_request(:post, "https://www.googleapis.com/oauth2/v4/token").to_return(status: 200, body: "{}", headers: { 'Content-Type' => 'application/json' })
      stub_request(:get, "https://sheets.googleapis.com/v4/spreadsheets/#{sheet_id}/values/Leading%20Organization%20Users")
        .to_return(
          status: 200,
          body: '{"range":"Leading Organization Users!A1:C1","majorDimension":"ROWS","values":[["username"],["leader"],["another_leader"],["final.leader"]]}',
          headers: { 'Content-Type' => 'application/json' })
    end

    context 'with GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH env var not defined' do
      it 'warns the variable is not defined and returns nil' do
        expect(subject).to receive_message_chain(:logger, :warn).with('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH env variable not defined.')

        expect(subject.leading_organization?(username)).to be_nil
      end
    end

    context 'with LEADING_ORGS_TRACKER_SHEET_ID env var not defined' do
      let(:credential_path) { 'path/to/credentials' }

      it 'warns the variable is not defined and returns nil' do
        expect(subject).to receive_message_chain(:logger, :warn).with('LEADING_ORGS_TRACKER_SHEET_ID env variable not defined.')

        expect(subject.leading_organization?(username)).to be_nil
      end
    end

    context 'with GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH env var defined' do
      let(:credential_path) { 'path/to/credentials' }
      let(:sheet_id) { 'SHEET_ID' }

      context 'with valid user id' do
        context 'when present in the CSV' do
          it 'returns true' do
            expect(subject.leading_organization?(username)).to be(true)
          end
        end

        context 'when present in the CSV in different case' do
          let(:username) { 'LeadeR' }

          it 'returns true' do
            expect(subject.leading_organization?(username)).to be(true)
          end
        end

        context 'when not present in the CSV' do
          let(:username) { regular_username }

          it 'returns false' do
            expect(subject.leading_organization?(username)).to be(false)
          end
        end
      end

      context 'when username is nil' do
        let(:username) { nil }

        it 'returns false' do
          expect(subject.leading_organization?(username)).to be(false)
        end
      end
    end
  end
end
