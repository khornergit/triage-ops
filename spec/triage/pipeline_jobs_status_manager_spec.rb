# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/pipeline_jobs_status_manager'

RSpec.describe Triage::PipelineJobsStatusManager do
  let(:api_client) { instance_double(Gitlab::Client) }
  let(:project_id) { 1 }
  let(:file_name)  { 'master-status.json' }
  let(:branch)     { 'master-status-branch' }

  let(:rubocop_status) do
    {
      "name" => "rubocop",
      "stage" => "test",
      "status" => "failed",
      "allow_failure" => false,
      "last_finished_at" => "2016-08-12 15:23:28 UTC",
      "last_failed" => {
        "web_url" => "rubocop_web_url",
        "finished_at" => "2016-08-12 15:23:28 UTC"
      }
    }
  end

  let(:file_content) { JSON.pretty_generate([rubocop_status]) }
  # rubocop:disable RSpec/VerifiedDoubles
  let(:response_double) do
    double('response',
      code: 404,
      request: double('request', base_uri: '', path: ''),
      parsed_response: { message: 'error' })
  end
  # rubocop:enable RSpec/VerifiedDoubles

  subject do
    described_class.new(
      api_client: api_client,
      project_id: project_id,
      file_name: file_name,
      branch_name: branch
    )
  end

  before do
    allow(api_client).to receive(:file_contents).and_return(file_content)
  end

  describe '#fetch_statuses_hash' do
    context 'with status file not found' do
      before do
        allow(api_client).to receive(:file_contents).and_raise(Gitlab::Error::NotFound.new(response_double))
      end

      it 'returns empty array' do
        expect(subject.fetch_statuses_hash).to eq([])
      end
    end

    context 'with status present' do
      it 'returns jobs statuses hash' do
        expect(subject.fetch_statuses_hash).to eq([rubocop_status])
      end
    end
  end

  describe '#update_and_publish' do
    let(:finished_at) { '2016-08-12 15:23:28 UTC' }
    let(:job_stage)   { 'test' }
    let(:job_status)  { 'failed' }

    let(:jobs) do
      [
        Gitlab::ObjectifiedHash.new(
          'name' => 'rubocop',
          'stage' => job_stage,
          'status' => job_status,
          'web_url' => "rubocop_web_url",
          'finished_at' => finished_at,
          'allow_failure' => false)
      ]
    end

    let(:commit_message) { 'Update status page' }

    context 'when status file not found' do
      before do
        allow(api_client).to receive(:file_contents).and_raise(Gitlab::Error::NotFound.new(response_double))
      end

      it 'creates the status file' do
        expect(api_client).to receive(:create_file).with(
          project_id,
          file_name,
          branch,
          file_content,
          commit_message
        )

        subject.update_and_publish(jobs_payload: jobs, commit_message: commit_message)
      end
    end

    context 'when the pipeline status page is present' do
      let(:job_status_base) do
        {
          "name" => "rubocop",
          "stage" => "test",
          "status" => "failed",
          "allow_failure" => false,
          "last_finished_at" => "2016-08-12 15:23:28 UTC",
          "last_failed" => {
            "web_url" => "rubocop_web_url",
            "finished_at" => "2016-08-12 15:23:28 UTC"
          }
        }
      end

      context 'when a finished job not found in status page due to mismatched name and stage' do
        let(:job_stage) { 'lint' }
        let(:updated_status_content) do
          JSON.pretty_generate(
            [job_status_base, job_status_base.merge("stage" => job_stage)]
          )
        end

        it 'adds the job status to the status page' do
          expect(api_client).to receive(:edit_file).with(
            project_id,
            file_name,
            branch,
            updated_status_content,
            commit_message
          )

          subject.update_and_publish(jobs_payload: jobs, commit_message: commit_message)
        end
      end

      context 'when a job name was previously reported on status page' do
        context 'with the finished job newer than the status page' do
          let(:finished_at) { '2020-08-12 15:23:28 UTC' }

          let(:updated_status_content) do
            JSON.pretty_generate([job_status_base.merge(
              "last_finished_at" => finished_at,
              "last_failed" => {
                "web_url" => "rubocop_web_url",
                "finished_at" => finished_at
              }
            )])
          end

          it 'updates the existing job with newer url and timestamp' do
            expect(api_client).to receive(:edit_file).with(
              project_id,
              file_name,
              branch,
              updated_status_content,
              commit_message
            )

            subject.update_and_publish(jobs_payload: jobs, commit_message: commit_message)
          end
        end

        context 'with the finished job older than the last status update' do
          let(:job_name)    { 'rubocop' }
          let(:finished_at) { '2016-08-12 15:23:25 UTC' }

          it 'does not update the status page' do
            expect(api_client).not_to receive(:edit_file)

            subject.update_and_publish(jobs_payload: jobs, commit_message: commit_message)
          end
        end

        context 'with the job payload missing a finished_at value (happens with untriggered manual jobs)' do
          let(:job_name)    { 'rubocop' }
          let(:finished_at) { nil }

          it 'does not update the status page' do
            expect(api_client).not_to receive(:edit_file)

            subject.update_and_publish(jobs_payload: jobs, commit_message: commit_message)
          end
        end

        context 'when pipeline status does not have last_finished_at value while job payload does' do
          let(:finished_at) { '2020-08-12 15:23:25 UTC' }
          let(:rubocop_status) do
            {
              "name" => "rubocop",
              "stage" => "test",
              "status" => "skipped",
              "allow_failure" => false,
              "last_finished_at" => nil
            }
          end

          let(:updated_status_content) do
            JSON.pretty_generate([
              job_status_base.merge(
                "status" => job_status,
                "last_finished_at" => finished_at,
                "last_failed" => {
                  "web_url" => "rubocop_web_url",
                  "finished_at" => finished_at
                }
              )
            ])
          end

          it 'updates status timestamp and history' do
            expect(api_client).to receive(:edit_file).with(
              project_id,
              file_name,
              branch,
              updated_status_content,
              commit_message
            )

            subject.update_and_publish(jobs_payload: jobs, commit_message: commit_message)
          end
        end

        context 'when finished job is newer and has a different(success) status' do
          let(:job_status)  { 'success' }
          let(:finished_at) { "2020-08-12 15:23:28 UTC" }

          let(:updated_status_content) do
            JSON.pretty_generate([
              job_status_base.merge(
                "status" => job_status,
                "last_finished_at" => finished_at,
                "last_succeeded" => {
                  "web_url" => "rubocop_web_url",
                  "finished_at" => finished_at
                }
              )
            ])
          end

          it 'updates last_succeeded data to status_history' do
            expect(api_client).to receive(:edit_file).with(
              project_id,
              file_name,
              branch,
              updated_status_content,
              commit_message
            )

            subject.update_and_publish(jobs_payload: jobs, commit_message: commit_message)
          end
        end
      end
    end
  end
end
