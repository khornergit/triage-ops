# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/event'
require_relative '../../triage/processor/team_label_inference'

RSpec.describe Triage::TeamLabelInference do
  include_context 'with event', Triage::IssueEvent do
    let(:object_kind)            { 'issue' }
    let(:action)                 { 'open' }
    let(:from_gitlab_org)        { true }
    let(:gitlab_bot_event_actor) { false }
    let(:label_names)            { ['group::group1'] }

    let(:event_attrs) do
      {
        object_kind: object_kind,
        action: action,
        from_gitlab_org?: from_gitlab_org,
        gitlab_bot_event_actor?: gitlab_bot_event_actor
      }
    end

    let(:comment_body_with_label_quick_action) do
      <<~MARKDOWN.chomp
        #{label_quick_action}
      MARKDOWN
    end

    let(:label_quick_action) { '/label ~"devops::stage with two groups" ~"section::section1"' }
  end

  shared_examples 'nothing happens' do
    it 'nothing happens' do
      expect_no_request do
        subject.process
      end
    end
  end

  subject { described_class.new(event) }

  before do
    stub_const("#{described_class}::UNSUPPORTED_DEVOPS_LABELS", ['devops::stage3'])
  end

  include_examples 'registers listeners', %w[issue.open issue.reopen issue.update merge_request.open merge_request.reopen merge_request.update]

  describe '#applicable?' do
    include_examples 'applicable on contextual event'

    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when event was authored by gitlab bot' do
      let(:gitlab_bot_event_actor) { true }

      include_examples 'event is not applicable'
    end

    context 'with Section label' do
      let(:label_names) { %w[section::section1] }

      include_examples 'event is not applicable'
    end

    context 'with Stage label' do
      let(:label_names) { %w[devops::stage_with_one_group] }

      include_examples 'event is applicable'
    end

    context 'with Group label' do
      let(:label_names) { ['group::group1'] }

      include_examples 'event is applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    context 'when no Group, Stage or Section is present' do
      let(:action) { 'update' }
      let(:label_names) { [] }

      it_behaves_like 'nothing happens'
    end

    context 'when only Group is present' do
      context 'with supported group label' do
        let(:label_names) { ['group::group1'] }
        let(:label_quick_action) { '/label ~"devops::stage with two groups" ~"section::section1"' }

        it 'infers Stage and Section labels' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'with unlisted group label' do
        let(:label_names) { ['Quality'] }

        it_behaves_like 'nothing happens'
      end

      context 'with unsupported group label' do
        let(:label_names) { ['group::pricing'] }

        it_behaves_like 'nothing happens'
      end
    end

    context 'when only Stage is present' do
      let(:action) { 'reopen' }
      let(:label_names) { ['devops::stage with two groups'] }
      let(:label_quick_action) { '/label ~"section::section1"' }

      context 'with supported Stage label' do
        it 'infers Section label' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end
    end

    context 'when only Section is present' do
      let(:label_names) { %w[section::section1] }

      it_behaves_like 'nothing happens'
    end

    context 'when only Stage and Group are present' do
      let(:object_kind) { 'merge_request' }
      let(:action) { 'update' }

      context 'when both group and Stage labels are unsupported' do
        let(:label_names) { %w[devops::stage3 group::pricing] }

        it_behaves_like 'nothing happens'
      end

      context 'with unsupported group label' do
        let(:label_names) { %w[devops::stage_with_one_group group::pricing] }
        let(:label_quick_action) { '/label ~"group::group3" ~"section::section2"' }

        it 'only infers the Section label from the supported Stage label' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'with unsupported Stage label' do
        let(:label_names) { %w[devops::stage3 group::group1] }
        let(:label_quick_action) { '/label ~"devops::stage with two groups" ~"section::section1"' }

        it 'infers both stage and section labels from the group label' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'when the Stage and Group match' do
        let(:label_names) { %w[devops::stage_with_one_group group::group3] }
        let(:label_quick_action) { '/label ~"Category:5" ~"section::section2"' }

        it 'sets the appropriate section and category label to align with the Group' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end

      context 'when the Stage and Group do not match' do
        let(:object_kind) { 'merge_request' }
        let(:action) { 'reopen' }
        let(:label_names) { %w[devops::stage_with_one_group group::group1] }

        it_behaves_like 'nothing happens'
      end
    end

    context 'when Section, Stage and Group and all are matching' do
      let(:label_names) { ['section::section1', 'devops::stage with two groups', 'group::group1'] }

      it_behaves_like 'nothing happens'
    end

    context 'when Section, Stage and Group are present, but Stage label is unsupported' do
      let(:label_names) { ['section::section1', 'devops::stage3', 'group::group1'] }
      let(:label_quick_action) { '/label ~"devops::stage with two groups"' }

      it 'sets the appropriate Stage label to align with Section and Group' do
        expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
          subject.process
        end
      end
    end

    context 'when Section, Stage and Group are present but Section and Stage are mismatched from Group' do
      let(:label_names) { ['section::section2', 'devops::stage_with_one_group', 'group::group1'] }

      it_behaves_like 'nothing happens'
    end

    context 'when Section, Stage and Group are present but Group label is unsupported' do
      context 'when Stage and Section labels match' do
        let(:label_names) { ['section::section1', 'devops::stage with two groups', 'group::pricing'] }

        it_behaves_like 'nothing happens'
      end

      context 'when Stage and Section labels do not match' do
        let(:label_names) { ['section::section2', 'devops::stage with two groups', 'group::pricing'] }

        it_behaves_like 'nothing happens'
      end
    end

    context 'when only Section and Group are present' do
      context 'with supported group label' do
        let(:label_names) { ['section::section1', 'group::group1'] }
        let(:label_quick_action) { '/label ~"devops::stage with two groups"' }

        it 'infers the Stage from Group' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end
    end

    context 'when only Section and Stage are present' do
      context 'when they are matching' do
        let(:label_names) { %w[section::section2 devops::stage_with_one_group] }
        let(:label_quick_action) { '/label ~"group::group3"' }

        it 'adds the group since the stage only has one group' do
          expect_comment_request(event: event, body: comment_body_with_label_quick_action) do
            subject.process
          end
        end
      end
    end

    context 'with invalid group name' do
      let(:label_names) { ['group::does not exist'] }

      it 'returns nil' do
        expect(subject.process).to be_nil
      end
    end

    context 'with invalid Stage label' do
      let(:label_names) { ['devops::does not exist'] }

      it 'returns nil' do
        expect(subject.process).to be_nil
      end
    end
  end
end
