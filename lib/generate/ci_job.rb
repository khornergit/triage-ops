# frozen_string_literal: true

require 'yaml'
require 'erb'
require 'fileutils'
require 'delegate'

require_relative '../hierarchy/group'
require_relative '../www_gitlab_com'

module Generate
  class CiJob
    class PeopleSetForCiJob < SimpleDelegator
      attr_accessor :policy_file
    end

    def self.run(options)
      new(template: options.template).run
    end

    def run
      prepare

      return if teams.empty?

      puts "Generating #{generated_jobs_path}"
      File.write(
        generated_jobs_path,
        erb.result_with_hash(teams: teams).squeeze("\n")
      )
    end

    attr_reader :template, :template_name, :policy_base

    def initialize(template:)
      @template = template
      @template_name = File.basename(template)
      @policy_base = "policies/generated/#{template_name}"
    end

    private

    def prepare
      FileUtils.rm_rf(File.join(destination, "#{template_name}.yml"), secure: true)
      FileUtils.mkdir_p(destination)
    end

    def teams
      @teams ||= policy_file_names.map { |file_name| team_for_file(file_name) }
    end

    def policy_file_names
      Dir["#{policy_base}/*.{yml,yaml}"]
        .map { |file_name| File.basename(file_name) }
        .sort
    end

    def team_for_file(file_name)
      name = File.basename(file_name, '.*')
      team =
        if Hierarchy::Group.exist?(name)
          Hierarchy::Group.new(name)
        elsif Hierarchy::Stage.exist?(name)
          Hierarchy::Stage.new(name)
        else
          raise "`#{name}` is not a valid key for a group nor a stage!"
        end

      PeopleSetForCiJob.new(team).tap { |job| job.policy_file = "#{policy_base}/#{file_name}" }
    end

    def erb
      @erb ||= ERB.new(File.read(template), trim_mode: '-')
    end

    def generated_jobs_path
      @generated_jobs_path ||= "#{destination}/#{template_name}.yml"
    end

    def destination
      @destination ||=
        File.expand_path('generated', "#{__dir__}/../../.gitlab/ci")
    end
  end
end
