# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../triage/related_issue_finder'

module Triage
  class AddQuarantineLabelsToRelatedIssue < Processor
    react_to 'merge_request.merge'

    def applicable?
      event.from_gitlab_org_gitlab? &&
        quarantine_label_set? &&
        related_issue
    end

    def process
      add_quarantine_labels_to_related_issue
    end

    def documentation
      <<~TEXT
        For merged MR, if the MR has the ~quarantine label set and a related issue,
        we add the ~quarantine and ~"quarantine::flaky" labels to the related issue.
      TEXT
    end

    private

    def related_issue
      @related_issue ||= Triage::RelatedIssueFinder.new.find_project_issue_in(event.project_id, event.description)
    end

    def quarantine_label_set?
      @quarantine_label ||= event.label_names.include?(Labels::QUARANTINE_LABEL)
    end

    def add_quarantine_labels_to_related_issue
      Triage.api_client.edit_issue(event.project_id, related_issue.iid, add_labels: [Labels::QUALITY_LABEL, Labels::QUARANTINE_FLAKY_LABEL])
    end
  end
end
